package com.jlh.f1racehistory;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.io.IOException;
import java.util.List;

public class RaceData extends AndroidViewModel {
    private F1Repository repository;
    private LiveData<List<ResultsInfo>> results;

    private int race_id;
    private List<RaceLapInfo> raceLapInfo;

    public RaceData(Application application) throws IOException {
        super(application);
        repository = new F1Repository(application);
       // results = repository.getAllCircuits();
    }

    public boolean RaceLoad(int race_id) {
        this.race_id = race_id;
        return true;
    }
}
