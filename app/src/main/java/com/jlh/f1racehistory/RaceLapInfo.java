package com.jlh.f1racehistory;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

public class RaceLapInfo {
    private int lapnumber;
    private List<LapTimesInfo> laptimes;

    public RaceLapInfo(int lapnumber, List<LapTimesInfo> laptimes) {
        this.lapnumber = lapnumber;
        this.laptimes = laptimes;
    }

    public int getLapnumber() {
        return lapnumber;
    }

    public void setLapnumber(int lapnumber) {
        this.lapnumber = lapnumber;
    }

    public List<LapTimesInfo> getLaptimes() {
        return laptimes;
    }

    public void setLaptimes(List<LapTimesInfo> laptimes) {
        this.laptimes = laptimes;
    }
}
