package com.jlh.f1racehistory.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jlh.f1racehistory.Circuits;
import com.jlh.f1racehistory.R;
import com.jlh.f1racehistory.RaceActivity;
import com.jlh.f1racehistory.RecyclerRaceAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private MainViewModel mViewModel;
    private RecyclerRaceAdapter raceAdapter;
    private TextView circuitId;
    private EditText circuitName;
    private EditText circuitLatitude;

    private String selectedYear = "2018";

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        Spinner spinner = getView().findViewById(R.id.spinnerYear);

        spinner.setOnItemSelectedListener(this);

        List<String> years = new ArrayList<String>();
        years.add("2018");
        years.add("2017");
        years.add("2016");
        years.add("2015");
        years.add("2014");
        years.add("2013");
        years.add("2012");
        years.add("2011");
        years.add("2010");
        years.add("2009");
        years.add("2008");
        years.add("2007");
        years.add("2006");
        years.add("2005");
        years.add("2004");
        years.add("2003");
        years.add("2002");
        years.add("2001");
        years.add("2000");

        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, years);

        spinner.setAdapter(adapter);

        listenerSetup();
        observerSetup();
        recyclerSetup();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        selectedYear = parent.getItemAtPosition(position).toString();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void listenerSetup() {

        Button addButton = getView().findViewById(R.id.addCircuit);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(getActivity(), "Showing Races for: " + selectedYear, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(view.getContext(), RaceActivity.class);
                intent.putExtra("year", selectedYear);
                startActivity(intent);
            }
        });
    }

    private void observerSetup() {

        // Circuits
        mViewModel.getSearchResultsCirciuts().observe(getViewLifecycleOwner(), new Observer<List<Circuits>>() {
            @Override
            public void onChanged(List<Circuits> circuits) {
                if (circuits.size() > 0) {
                    circuitId.setText(String.format(Locale.US, "%d", circuits.get(0).getId()));
                    circuitName.setText(circuits.get(0).getName());
                    circuitLatitude.setText("0.0");
                } else {
                    circuitId.setText("No Match");
                }
            }
        });

        // Constructor Results
        // Constructors
        // Constructor Standings
        // Drivers
        // Driver Standings
        // Lap Times
        // Pit Stops
        // Qualifying
        // Races
        // Results
        // Seasons
        // Status
    }

    public void recyclerSetup() {

    }

    private void clearFields() {
        circuitId.setText("");
        circuitName.setText("");
        circuitLatitude.setText("");
    }

}