package com.jlh.f1racehistory;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class RecyclerRaceAdapter extends RecyclerView.Adapter<RecyclerRaceAdapter.ViewHolder> {

    private List<Races> racesList;
    private List<RacesInfo> racesListByYear;

    public void setRacesList(List<Races> races) {
        racesList = races;
        notifyDataSetChanged();
    }
    public void setRacesListByYear(List<RacesInfo> races) {
        racesListByYear = races;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.racelist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemName.setText(racesListByYear.get(position).getName());
        holder.itemRaceNumber.setText(String.valueOf(position + 1));
        holder.itemCircuit.setText(racesListByYear.get(position).getCircuit());
        holder.itemLocation.setText(racesListByYear.get(position).getLocation() + ", " +
                racesListByYear.get(position).getCountry());
    }

    @Override
    public int getItemCount() {
        if (racesListByYear != null) {
            return racesListByYear.size();
        } else {
            return  0;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView itemRaceNumber;
        TextView itemCircuit;
        TextView itemLocation;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.racelist_name);
            itemRaceNumber = itemView.findViewById(R.id.racelist_racenumber);
            itemCircuit = itemView.findViewById(R.id.racelist_circuit);
            itemLocation = itemView.findViewById(R.id.racelist_location);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    TextView race = view.findViewById(R.id.racelist_name);

                    // Snackbar.make(view, "Selected Race: " + (position + 1) + " - " + race.getText(), Snackbar.LENGTH_LONG).setAction("Action", null).show();

                    Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                    view.getContext().startActivity(intent);
                }
            });
        }
    }

}
